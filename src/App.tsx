"use strict";
import * as React from 'react';
import './App.css';

import logoGM from "./assets/logo_gm.png"
import logoReact from './logo.svg';

import { ButtonToolbar, Dropdown, SplitButton } from "react-bootstrap";

import { ReleaseList, ReleaseType } from "./components/Release"

// @ts-ignore
import { Goober, TabView } from "./components/TabView"

// @ts-ignore
import { Tab } from "./components/Tab"

// @ts-ignore
class TestUI extends React.Component {
	public render() {
		const btnPlay = <SplitButton
			bsStyle="default"
			title="Play"
			id={`split-button-play`}
		>
			<Dropdown.Item eventKey="1">Action</Dropdown.Item>
			<Dropdown.Item eventKey="2">Another action</Dropdown.Item>
			<Dropdown.Item eventKey="3">Something else here</Dropdown.Item>
			<Dropdown.Item divider={true} />
			<Dropdown.Item eventKey="4">Separated link</Dropdown.Item>
		</SplitButton>

		const btnCreate = <SplitButton
			bsStyle="default"
			title="Create"
			id={`split-button-create`}
		>
			<Dropdown.Item eventKey="1">Action</Dropdown.Item>
			<Dropdown.Item eventKey="2">Another action</Dropdown.Item>
			<Dropdown.Item eventKey="3">Something else here</Dropdown.Item>
			<Dropdown.Item divider={true} />
			<Dropdown.Item eventKey="4">Separated link</Dropdown.Item>
		</SplitButton>

		return (
			<div>
				<ButtonToolbar>
					{btnPlay}
					{btnCreate}
				</ButtonToolbar>

				<ReleaseList type={ReleaseType.Game_Mod} />
				<ReleaseList type={ReleaseType.ModTools} />
			</div>
		);
	}
}

//
// Actual Layout
//
class App extends React.Component {
	public render() {
		const logo = (
			<img src={(() => {
				if (process.env.NODE_ENV !== 'production') {
					return logoReact;
				}
				return logoGM;
			})()} className="App-logo" alt="logo" />
		);

		return (
			<div className="App">
				<header className="App-header">
					{logo}
					<div className="App-categories">
						<TabView>
							<Tab label="Play" active={true}>
								<ReleaseList type={ReleaseType.Game_Mod} />
							</Tab>
							<Tab label="Create">
								<ReleaseList type={ReleaseType.ModTools} />
							</Tab>
						</TabView>
					</div>
				</header>
			</div>
		);
	}
}

export default App;
