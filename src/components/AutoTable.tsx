import * as React from 'react';


/**
 * Enum which defines the header style for the table
 *
 * @export
 * @enum {number}
 */
export enum AutoTableStyle {
	Default,
	UpperCase,
	LowerCase,
}


/**
 * Automatically an AutoTableStyle to given text
 *
 * @param {string} text
 * @param {AutoTableStyle} [style]
 * @returns
 */
function ApplyAutoTableStyle(text: string, style?: AutoTableStyle) {
	const applyStyle = {
		[AutoTableStyle.Default]: (str: string) => str,
		[AutoTableStyle.UpperCase]: (str: string) => str.toUpperCase(),
		[AutoTableStyle.LowerCase]: (str: string) => str.toLowerCase(),
	}

	return applyStyle[style || AutoTableStyle.Default](text);
}


/**
 * @interface IAutoTableProps
 * @template T
 */
interface IAutoTableProps<T> {
	data: T[];
	headerStyle?: AutoTableStyle
}


/**
 * Automatically generates a table based on the given array of objects
 *
 * @export
 * @class AutoTable
 * @extends {React.Component<IAutoTableProps<T>>}
 * @template T
 * 
 * @example
 * import { AutoTable, AutoTableStyle } from "./AutoTable"
 * // ...
 * <AutoTable data={tableData} headerStyle={AutoTableStyle.UpperCase} />
 */
export class AutoTable<T> extends React.Component<IAutoTableProps<T>> {
	public render() {
		const {
			props: {
				data,
				headerStyle
			}
		} = this;

		const keys: string[] = [];
		for (const key in data[0]) {
			if (keys.hasOwnProperty(key) || 1) {
				keys.push(key)
			}
		}

		const labels = (<tr>
			{keys.map((value, index) =>
				(<th key={index}>{ApplyAutoTableStyle(value, headerStyle)}</th>))}
		</tr>);

		const values = data.length > 0 ? data.map((rowValue, rowIndex) => {
			const rowContent = keys.map((name, valueIndex) =>
				(<td key={valueIndex}>{(rowValue[name] || "")}</td>));
			return (<tr key={rowIndex}>{rowContent}</tr>);
		}) : undefined;

		return (
			<table >
				<tbody>
					{labels}
					{values}
				</tbody>
			</table>
		);
	}
}