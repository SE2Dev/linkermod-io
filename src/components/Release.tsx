import Octokit from "@octokit/rest";
import * as React from 'react';

import * as LocalCache from "../util/localCache"

import Octicon from 'react-component-octicons'

import { AutoTable, AutoTableStyle } from "./AutoTable"

// @ts-ignore (ignore cannot use new on Octokit type error)
const octokit: Octokit = new Octokit();

type ReleaseInfo = Octokit.ReposListReleasesResponseItem;

/**
 * Identifies the release type for a given release
 *
 * @export
 * @enum {number}
 */
export enum ReleaseType {
	Game_Mod,
	ModTools,
	Other,
}

/**
 * Resolve the {ReleaseType} type from a given {ReleaseInfo}
 *
 * @param {ReleaseInfo} info
 * @returns
 */
function ResolveReleaseType(info: ReleaseInfo) {
	// Resolve the release type
	if (info.tag_name.endsWith("-r")) {
		return ReleaseType.ModTools;
	} else {
		// TODO: Advanced tag system for other types of releases
		return ReleaseType.Game_Mod;
	}
}

//
// Start the release fetching process early.
// Provide promise for future access to the results
//
const releasePromise = (async () => {
	const cookie = "releases";
	const cachedReleases = LocalCache.get<ReleaseInfo[] | null>(cookie) || [];

	// Use cached resource list cookie if it's available
	// to prevent hitting the API request limit (60 requests / hr)
	if (cachedReleases.length > 0) {
		if (process.env.NODE_ENV === 'development') {
			console.log("Using cached release cookie.");
		}
		return cachedReleases;
	}

	// DEPRECATED: Hard release cache for development
	/* if (process.env.NODE_ENV !== 'production') {
		return require("../dev/releases");
	} */

	// If we were unable to retrieve the cached release list,
	// then we need to retrieve it (via octokit)
	// and store it as a cookie (via localStorage)

	if (process.env.NODE_ENV === 'development') {
		console.log("Retrieving release list.");
	}

	return octokit.repos.listReleases({
		owner: "Nukem9",
		repo: "LinkerMod"
	}).then((response) => {
		const releases = response.data;

		// Cache the release list using cookies (expires after 60 minutes)
		// Note: Date.setMinutes() automatically handles input
		//       values that are out of range - 60 min -> 1 hr, etc.
		const expires = new Date();
		expires.setMinutes(expires.getMinutes() + 60);
		LocalCache.set(cookie, releases, expires);

		if (process.env.NODE_ENV === 'development') {
			console.log("Stored release cookie.");
		}

		return releases;
	}).catch(err => {
		console.error(err);
		return [];
	});
})();

interface IReleaseInfoProps {
	release: ReleaseInfo;
}

export class Release extends React.Component<IReleaseInfoProps> {
	constructor(info: IReleaseInfoProps) {
		super(info);
	}

	public render() {
		return (
			<div >
				<Octicon name="alert" />
				{this.props.release.name}
			</div>
		);
	}
}

interface IReleaseListProps {
	type: ReleaseType
}

export class ReleaseList extends React.Component<IReleaseListProps, { releases: ReleaseInfo[] }> {
	private _isMounted = false;

	public render() {
		if (this.state === null) {
			return <AutoTable data={[]} />
		}

		const tableData = this.state.releases.filter((info: ReleaseInfo) =>
			(ResolveReleaseType(info) === this.props.type)
		).map((r) => {
			/* tslint:disable */
			return {
				name: r.name,
				version: r.tag_name,
				date: (new Date(r.published_at).toLocaleString()),
				link: (<a href={r.html_url}>more info</a>)
			}
		});

		return (<div>
			<AutoTable data={tableData} headerStyle={AutoTableStyle.UpperCase} />
			<Octicon name="desktop-download" />
			<Octicon name="file-zip" />
			<Octicon name="desktop-download" />
		</div>)
	}

	public async componentDidMount() {
		// Mark this component as mounted
		this._isMounted = true;

		const releases = await releasePromise;

		// Safety check - because the component could potentially unmount
		// before our promise was resolved
		if (this._isMounted) {
			this.setState({
				releases
			});
		}
	};

	public componentWillUnmount() {
		// Mark the component as unmounted
		this._isMounted = false;
	}
}
