import * as React from 'react';

export interface ITabProps {
	label: string;
	active?: boolean;
	children?: React.ReactNode;
	onClick?: (label: string) => void;
}


export class Tab extends React.Component<ITabProps> {
	/* static propTypes = {
	  activeTab: PropTypes.string.isRequired,
	  label: PropTypes.string.isRequired,
	  onClick: PropTypes.func.isRequired,
	}; */

	constructor(props: ITabProps) {
		super(props);

		// This binding is necessary to make `this` work 
		// in the callback
		this.onClick = this.onClick.bind(this);
	}

	public onClick() {
		const { label, onClick } = this.props;
		if (!onClick) {
			return;
		}
		onClick(label);
	}


	/**
	 * Render this tab's label
	 *
	 * @returns
	 * @memberof Tab
	 */
	public render() {
		let className = 'tab-list-item';

		if (this.props.active) {
			className += ' tab-list-active';
		}

		return (
			<li className={className}
				onClick={this.onClick}>
				{this.props.label}
			</li>
		);
	}
}
