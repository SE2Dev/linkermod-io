import * as React from "react";

import { ITabProps, Tab } from "./Tab"

// https://alligator.io/react/tabs-component/

export interface ITabViewProps {
	// children: Array<React.ReactElement<IGooberProps>>;
	children: Array<React.ReactElement<ITabProps>>;
}

interface ITabViewState {
	activeTab: React.ReactElement<ITabProps> | null;
}

export class TabView extends
	React.Component<ITabViewProps, ITabViewState>{

	constructor(props: ITabViewProps) {
		super(props);

		// state must be set directly since the component
		// hasn't been mounted yet
		if (!this.props.children) {
			this.state = { activeTab: null };
		} else {
			for (const tab of this.props.children) {
				if (!tab.props.active) {
					continue;
				}
				this.state = {
					activeTab: tab
				}
				break;
			}
		}




		// This binding is necessary to make `this` work 
		// in the callback
		this.activateTab = this.activateTab.bind(this);
	}

	public render() {
		const {
			props: {
				children,
			},
			state: {
				activeTab,
			}
		} = this;

		return (
			<div className="tabs">
				<ol className="tab-list">
					{
						children.map((child, index) => {
							return (
								<Tab
									key={index}
									label={child.props.label}
									active={child === activeTab}
									onClick={this.activateTab}
								/>
							);
						})
					}
				</ol>
				<div className="tab-content">
					{
						// Resolve the tab content
						(() => {
							if (activeTab === null) {
								return;
							}

							return activeTab.props.children;
						})()
					}
				</div>
			</div>
		);
	}

	private activateTab(label: string) {
		//
		// Resolve the active tab by label
		//
		for (const tab of this.props.children) {
			if (tab.props.label === label) {
				return this.setState({
					activeTab: tab
				});
			}
		}

		//
		// Fallback to null selection
		//
		return this.setState({
			activeTab: null
		});
	}


}