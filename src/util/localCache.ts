// Stupid polyfill for node tests
if (localStorage === undefined) {
	var localStorage = {
		getItem: (key: string) => null,
		setItem: (key: string, value: string) => null,
	};
}


/**
 * Simple interface that defines a cached storage item
 *
 * @interface ICacheItem
 * @template T
 */
interface ICacheItem<T> {

	/**
	 * Actual cached value
	 *
	 * @type {(T | null)}
	 * @memberof ICacheItem
	 */
	value?: T | null;


	/**
	 * Optional expiration date for this cache item
	 *
	 * @type {Date}
	 * @memberof ICacheItem
	 */
	expires?: Date;
}


/**
 * Attempt to retrieve an ICacheItem<T> from localStorage
 * Returns the object on success
 * Returns null if the item doesn't exist, or is expired
 *
 * @export
 * @template T
 * @param {string} key
 * @returns {(T | null)}
 */
export function get<T>(key: string): T | null {
	if (!localStorage) {
		console.warn("localStorage is unsupported!");
		return null;
	}

	const item = localStorage.getItem(key);
	if (item === null) {
		return null;
	}

	const cacheItem = JSON.parse(item) as ICacheItem<T>;
	if (!!cacheItem.expires &&
		(new Date(cacheItem.expires)).getTime() - (new Date()).getTime() < 0) {
		// The item is expired
		return null;
	}

	return cacheItem.value || null;
}


/**
 * Set a cached value via localStorage
 *
 * @export
 * @template T
 * @param {string} key
 * @param {T} value - actual value of the item
 * @param {Date} [expires] - optional expiration time for the cache item
 */
export function set<T>(key: string, value: T, expires?: Date) {
	if (!localStorage) {
		console.warn("localStorage is unsupported!");
		return;
	}

	const cacheItem: ICacheItem<T> = {
		expires,
		value,
	}
	localStorage.setItem(key, JSON.stringify(cacheItem));
}